#+TITLE: Dired commands
#+TITLE: Evil Commands + Emacs Utilities
#+AUTHOR: Zera Zelix
#+EMAIL: zerazelix@gmail.com
#+DATE: 2021-12-11 Friday
#+LANGUAGE: en
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="styles.css" />
#+OPTIONS: html-style:nil toc:nil num:nil

* Dired

** Copy
 C

** Rename/Move
 R

** Delete
 D + x

** Select
 m

** Deselect
 u

* PDF

** Down
   C-j

** Up
   C-k

** Specific page
   J

** Zoom in
   C-x + C-+

** Zoom out
   =C-x= + =C--=

* EWW

** Do a search
   M-x eww

** Click a link
   RET

** Go back
   Backspace
